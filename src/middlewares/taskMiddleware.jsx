import { createAsyncThunk } from "@reduxjs/toolkit";
import { projectServices } from "../services/project.service";
import { taskService } from "../services/task.service";

export const fetchProjectData = createAsyncThunk(
  "fetchProjectData",
  async () => {
    const res = await projectServices.getAllProject();
    return res.data.content;
  }
);
export const fetchStatusData = createAsyncThunk("fetchStatusData", async () => {
  let res = await taskService.getAllStatus();
  return res.data.content;
});

export const fetchPrioritesData = createAsyncThunk(
  "fetchPrioritesData",
  async () => {
    let res = await taskService.getAllPriorities();
    return res.data.content;
  }
);

export const fetchTypesData = createAsyncThunk("fetchTypesData", async () => {
  let res = await taskService.getTaskTypes();
  return res.data.content;
});
