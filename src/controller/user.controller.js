import { message } from "antd";
import { userServices } from "../services/user.service";

const getAllUsers = async () => {
  try {
    const res = await userServices.getAllUsers();
    return res.data.content;
  } catch (error) {
    message.error(error.message);
  }
};

// delete user
const deleteUser = async (userId) => {
  try {
    await userServices.deleteUser({ id: userId });
    message.success("Xóa thành công");
    return null;
  } catch (error) {
    message.error("Xóa thất bại");
  }
};
const editUser = async (data) => {
  try {
    await userServices.editUser(data);
    message.success("Chỉnh sửa thành công");
  } catch (error) {
    message.error(error.message);
  }
};
const getUserByProjectId = async (idProject) => {
  try {
    const res = await userServices.getUserByProject({ idProject });
    return res.data.content;
  } catch (error) {
    return null;
  }
};
export const userController = {
  getAllUsers,
  deleteUser,
  editUser,
  getUserByProjectId,
};
