import { notification } from "antd";

export const openNotification = (type, description) => {
  let message = "";
  switch (type) {
    case "success":
      message = "Success";
      break;
    case "error":
      message = "Error";
      break;
    default:
      break;
  }
  notification[type]({
    message: message,
    description: description,
  });
};
