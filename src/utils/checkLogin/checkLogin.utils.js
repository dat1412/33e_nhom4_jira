import { userInfoLocal } from "../../services/local.service";

export const checkLogin = (navigate, page) => {
  let _userInfoLocal = userInfoLocal.get();
  if (_userInfoLocal) {
    navigate(`/${page}`);
  } else {
    navigate("/login");
  }
};
