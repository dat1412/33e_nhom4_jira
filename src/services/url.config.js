import axios from "axios";
import { store } from "../index.js";
import { offLoading, onLoading } from "../redux/spinnerSlice";
import { userInfoLocal } from "./local.service";

export const BASE_URL = "https://jiranew.cybersoft.edu.vn";
const TOKEN_CYBERSOFT =
  "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ0ZW5Mb3AiOiJCb290Y2FtcCAzM0UiLCJIZXRIYW5TdHJpbmciOiIyNi8wNC8yMDIzIiwiSGV0SGFuVGltZSI6IjE2ODI0NjcyMDAwMDAiLCJuYmYiOjE2NTQzNjIwMDAsImV4cCI6MTY4MjYxNDgwMH0.GGqFf8-ZXIqAjnaJZ40LjQvUHb1VyvRv3XtEIsMe_qE";

export const configHeader = () => {
  return {
    TokenCybersoft: TOKEN_CYBERSOFT,
    Authorization: "Bearer " + userInfoLocal.get()?.accessToken,
  };
};

// Axios instan
export const https = axios.create({
  baseURL: BASE_URL,
  headers: {
    TokenCybersoft: TOKEN_CYBERSOFT,
    Authorization: "Bearer " + userInfoLocal.get()?.accessToken,
  },
});

// Add a request interceptor
https.interceptors.request.use(
  function (config) {
    const accessToken = userInfoLocal.get()?.accessToken;
    if (accessToken) {
      config.headers.Authorization = "Bearer " + accessToken;
    }

    // On Loadindg
    store.dispatch(onLoading());
    return config;
  },
  function (error) {
    // Do something with request error
    return Promise.reject(error);
  }
);

// Add a response interceptor
https.interceptors.response.use(
  function (response) {
    // Off Loading
    store.dispatch(offLoading());
    return response;
  },
  function (error) {
    // Any status codes that falls outside the range of 2xx cause this function to trigger
    // Do something with response error
    // Off Loading
    store.dispatch(offLoading());

    return Promise.reject(error);
  }
);
