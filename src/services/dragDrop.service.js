import axios from "axios";
import { BASE_URL, configHeader } from "./url.config";

export const dragDropServices = {
  updateStatus: (data) => {
    return axios({
      url: `${BASE_URL}/api/Project/updateStatus`,
      method: "PUT",
      data: data,
      headers: configHeader(),
    });
  },
  getProjectDetail: (params) => {
    return axios({
      url: `${BASE_URL}/api/Project/getProjectDetail`,
      method: "GET",
      params: params,
      headers: configHeader(),
    });
  },
};
