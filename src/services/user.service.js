import { https } from "./url.config";

export const userServices = {
  postLogin: (dataLogin) => {
    let uri = "/api/Users/signin";
    return https.post(uri, dataLogin);
  },
  logout: () => {
    localStorage.removeItem("USER_INFO");
  },
  postRegister: (dataRegister) => {
    let uri = "/api/Users/signup";
    return https.post(uri, dataRegister);
  },
  getAllUsers: () => {
    let uri = "/api/Users/getUser";
    return https.get(uri);
  },
  deleteUser: (params) => {
    let uri = `/api/Users/deleteUser`;
    return https.delete(uri, { params });
  },
  editUser: (data) => {
    let uri = "/api/Users/editUser";
    return https.put(uri, data);
  },
  getUserByProject: (params) => {
    let uri = `/api/Users/getUserByProjectId`;
    return https.get(uri, { params });
  },
};
