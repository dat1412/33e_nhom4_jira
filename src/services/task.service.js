import { https } from "./url.config";

export const taskService = {
  getAllStatus: () => {
    let uri = "/api/Status/getAll";
    return https.get(uri);
  },
  getAllPriorities: () => {
    let uri = `api/Priority/getAll`;
    return https.get(uri);
  },
  getTaskTypes: () => {
    let uri = `api/TaskType/getAll`;
    return https.get(uri);
  },
  createTask: (body) => {
    let uri = `api/Project/createTask`;
    return https.post(uri, body);
  },
  updateTask: (body) => {
    let uri = `api/Project/updateTask`;
    return https.post(uri, body);
  },
  getComment: (params) => {
    let uri = "api/Comment/getAll";
    return https.get(uri, { params });
  },
  postComment: (body) => {
    let uri = "api/Comment/insertComment";
    return https.post(uri, body);
  },
  deleteComment: (params) => {
    let uri = "api/Comment/deleteComment";
    return https.delete(uri, { params });
  },
};
