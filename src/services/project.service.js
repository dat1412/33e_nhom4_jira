import { https } from "./url.config";

export const projectServices = {
  createProject: (dataProject) => {
    let uri = "/api/Project/createProjectAuthorize";
    return https.post(uri, dataProject);
  },
  getAllProject: () => {
    let uri = "/api/Project/getAllProject";
    return https.get(uri);
  },
  deleteProject: (params) => {
    let uri = "/api/Project/deleteProject";
    return https.delete(uri, { params });
  },
  updateProject: (params, dataProject) => {
    let uri = "/api/Project/updateProject";
    return https.put(uri, dataProject, { params });
  },
  assignUserProject: (data) => {
    let uri = "/api/Project/assignUserProject";
    return https.post(uri, data);
  },
  removeUserFromProject: (data) => {
    let uri = "/api/Project/removeUserFromProject";
    return https.post(uri, data);
  },
  getProjectDetail: (params) => {
    let uri = "/api/Project/getProjectDetail";
    return https.get(uri, { params });
  },
};
