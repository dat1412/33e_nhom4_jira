import axios from "axios";
import { BASE_URL, configHeader } from "./url.config";

export const searchServices = {
  getUser: (keyword) => {
    return axios({
      url: `${BASE_URL}/api/Users/getUser?keyword=${keyword}`,
      method: "GET",
      headers: configHeader(),
    });
  },
  getProject: (keyword) => {
    return axios({
      url: `${BASE_URL}/api/Project/getAllProject?keyword=${keyword}`,
      method: "GET",
      headers: configHeader(),
    });
  },
};
