import UsersPage from "../pages/UsersPage/UsersPage";
import HomePage from "../pages/HomePage/HomePage";
import CreateTaskPage from "../pages/TasksPage/CreateTaskPage/CreateTaskPage";
import CreateProjectPage from "../pages/ProjectsPage/CreateProjectPage/CreateProjectPage";
import ProjectManagementPage from "../pages/ProjectsPage/ProjectManagementPage/ProjectManagementPage";
import ProjectDetailPage from "../pages/ProjectsPage/ProjectDetailPage/ProjectDetailPage";
import TaskDetailPage from "../pages/TasksPage/CreateTaskPage/TaskDetailPage";

export const routes = [
  {
    key: 1,
    component: <ProjectManagementPage />,
    path: "/projects",
    name: "Projects Management",
    configHeader: false,
  },
  {
    key: 2,
    component: <UsersPage />,
    path: "/users",
    name: "Users Management",
    configHeader: false,
  },
  {
    key: 3,
    component: <CreateProjectPage />,
    path: "/project/new",
    name: "Create Project",
    configHeader: true,
  },
  {
    key: 4,
    component: <CreateTaskPage />,
    path: "/task/new",
    name: "Create Task",
    configHeader: true,
  },
  {
    key: 5,
    component: <TaskDetailPage />,
    path: "/task/detail",
    name: "Task Detail",
    configHeader: false,
  },
  {
    key: 6,
    component: <ProjectDetailPage />,
    path: "/project/detail/:id",
    name: "Project Detail",
    configHeader: false,
  },
  {
    key: 7,
    component: <HomePage />,
    path: "/",
  },
];
