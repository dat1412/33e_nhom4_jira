import React, { useEffect } from "react";
import { Button, Drawer, Form, Input, Select, Space } from "antd";
import { useDispatch, useSelector } from "react-redux";
import { closeEditProject } from "../../redux/editProjectSlice";
import { projectServices } from "../../services/project.service";
import { openNotification } from "../../utils/notification/notification";
const { Option } = Select;
export default function FormEditProject({ fetchDataProject }) {
  let dispatch = useDispatch();
  const [form] = Form.useForm();
  const { isOpen, projectInfo } = useSelector(
    (state) => state.editProjectSlice
  );

  useEffect(() => {
    if (projectInfo) {
      var { id, projectName, description, categoryId } = projectInfo;
      form.setFieldsValue({
        id,
        projectName,
        description,
        categoryId,
      });
    }
  }, [projectInfo]);

  const onClose = () => {
    dispatch(closeEditProject());
  };

  const onFinish = (values) => {
    let params = {
      projectId: values.id,
    };

    // API
    projectServices
      .updateProject(params, values)
      .then((res) => {
        fetchDataProject();
        openNotification(
          "success",
          "Your Project has been updated successfully!"
        );
      })
      .catch((err) => {
        openNotification("error", "The update failed!");
      });
  };

  return (
    <>
      <Drawer
        title="Update project"
        width={window.innerWidth > 900 ? 700 : window.innerWidth - 100}
        onClose={onClose}
        open={isOpen}
        bodyStyle={{
          paddingBottom: 80,
        }}
      >
        <Form
          initialValues={{
            remember: true,
          }}
          form={form}
          layout="vertical"
          onFinish={onFinish}
        >
          <Form.Item
            name="id"
            label="Project ID"
            rules={[
              {
                required: true,
                message: "Please input your project id!",
              },
            ]}
          >
            <Input disabled />
          </Form.Item>
          <Form.Item
            name="projectName"
            label="Project Name"
            rules={[
              {
                required: true,
                message: "Please input your project name!",
              },
            ]}
          >
            <Input placeholder="Please input your project name!" />
          </Form.Item>
          <Form.Item
            name="categoryId"
            label="Category Name"
            rules={[
              {
                required: true,
                message: "Please select your category name!",
              },
            ]}
          >
            <Select placeholder="Select a project category" allowClear>
              <Option value={1}>Web</Option>
              <Option value={2}>Software</Option>
              <Option value={3}>App</Option>
            </Select>
          </Form.Item>
          <Form.Item name="description" label="Description">
            <Input.TextArea
              rows={7}
              placeholder="Please input your description!"
            />
          </Form.Item>
          <Form.Item>
            <Space>
              <Button onClick={onClose}>Cancel</Button>
              <Button onClick={onClose} type="primary" htmlType="submit">
                Update
              </Button>
            </Space>
          </Form.Item>
        </Form>
      </Drawer>
    </>
  );
}
