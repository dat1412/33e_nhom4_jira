import React, { useState } from "react";
import {
  FileOutlined,
  PieChartOutlined,
  UserOutlined,
  LogoutOutlined,
} from "@ant-design/icons";
import { Avatar, Dropdown, Layout, Menu, theme } from "antd";
import { useNavigate } from "react-router-dom";
import { useMediaQuery } from "react-responsive";
import MobileMenu from "./MobileMenu";
import { userInfoLocal } from "../../services/local.service";

const { Sider } = Layout;

export default function Dashboard({ children, route }) {
  const [collapsed, setCollapsed] = useState(false);
  function getItem(label, key, icon, children) {
    return {
      key,
      icon,
      children,
      label,
    };
  }
  const userInfo = userInfoLocal.get();
  const items = [
    getItem("Project", "sub1", <PieChartOutlined />, [
      getItem("View All Project", "1"),
      getItem("Create Project", "2"),
    ]),
    getItem("User", "sub2", <UserOutlined />, [getItem("View All User", "3")]),
    getItem("Task", "sub3", <FileOutlined />, [getItem("Create Task", "4")]),
  ];
  const getPathFromKey = (key) => {
    switch (key) {
      case "1":
        return "/projects";
      case "2":
        return "/project/new";
      case "3":
        return "/users";
      case "4":
        return "/task/new";

      default:
        return null;
    }
  };
  const userTab = [
    {
      label: (
        <p
          onClick={() => {
            userInfoLocal.remove();
            navigate("/login");
          }}
          className="flex justify-around items-center space-x-2 w-20 font-medium z-20"
        >
          <span className="flex justify-center items-center text-lg text-red-500">
            <LogoutOutlined />
          </span>
          <span>Sign out</span>
        </p>
      ),
      key: "1",
    },
  ];
  const {
    token: { colorBgContainer },
  } = theme.useToken();
  const isMobile = useMediaQuery({ query: "(max-width: 640px)" });
  const navigate = useNavigate();
  let _configHeader = route.configHeader;
  let widthHeader = "";
  if (_configHeader) {
    widthHeader = "w-3/4 mx-auto";
  } else {
    widthHeader = "";
  }
  return (
    <Layout
      style={{
        minHeight: "100vh",
      }}
    >
      {isMobile ? (
        <MobileMenu />
      ) : (
        <Sider
          collapsed={collapsed}
          onCollapse={(value) => setCollapsed(value)}
          className="z-20"
        >
          <div
            onClick={() => {
              navigate("/projects");
            }}
            className="flex justify-between items-center px-4 py-3 m-1 cursor-pointer"
          >
            <img
              className="w-1/3"
              src="https://www.vectorlogo.zone/logos/atlassian_jira/atlassian_jira-icon.svg"
              alt=""
            />
          </div>

          <Menu
            theme="dark"
            defaultSelectedKeys={["1"]}
            mode="inline"
            items={items}
            onClick={(e) => {
              const path = getPathFromKey(e.key);
              path && navigate(path);
            }}
            style={{
              textAlign: "left",
            }}
          />
        </Sider>
      )}
      <Layout
        className="site-layout"
        style={{
          backgroundColor: "#ffffff",
          backgroundImage: `url(
            "data:image/svg+xml,%3Csvg xmlns='http://www.w3.org/2000/svg' viewBox='0 0 1600 900'%3E%3Cdefs%3E%3ClinearGradient id='a' x1='0' x2='0' y1='1' y2='0' gradientTransform='rotate(144,0.5,0.5)'%3E%3Cstop offset='0' stop-color='%230FF'/%3E%3Cstop offset='1' stop-color='%23CF6'/%3E%3C/linearGradient%3E%3ClinearGradient id='b' x1='0' x2='0' y1='0' y2='1' gradientTransform='rotate(106,0.5,0.5)'%3E%3Cstop offset='0' stop-color='%23F00'/%3E%3Cstop offset='1' stop-color='%23FC0'/%3E%3C/linearGradient%3E%3C/defs%3E%3Cg fill='%23FFF' fill-opacity='0' stroke-miterlimit='10'%3E%3Cg stroke='url(%23a)' stroke-width='13.86'%3E%3Cpath transform='translate(-69.3 -17.200000000000003) rotate(-19.6 1409 581) scale(0.8361679999999999)' d='M1409 581 1450.35 511 1490 581z'/%3E%3Ccircle stroke-width='4.620000000000001' transform='translate(-163.5 75) rotate(-7.000000000000001 800 450) scale(0.978768)' cx='500' cy='100' r='40'/%3E%3Cpath transform='translate(-26.9 40.5) rotate(-99.5 401 736) scale(0.978768)' d='M400.86 735.5h-83.73c0-23.12 18.74-41.87 41.87-41.87S400.86 712.38 400.86 735.5z'/%3E%3C/g%3E%3Cg stroke='url(%23b)' stroke-width='4.2'%3E%3Cpath transform='translate(450 29) rotate(-7.25 150 345) scale(1.0420559999999999)' d='M149.8 345.2 118.4 389.8 149.8 434.4 181.2 389.8z'/%3E%3Crect stroke-width='9.240000000000002' transform='translate(24.5 -217) rotate(-154.8 1089 759)' x='1039' y='709' width='100' height='100'/%3E%3Cpath transform='translate(-119.6 8.399999999999999) rotate(-25.799999999999997 1400 132) scale(0.705)' d='M1426.8 132.4 1405.7 168.8 1363.7 168.8 1342.7 132.4 1363.7 96 1405.7 96z'/%3E%3C/g%3E%3C/g%3E%3C/svg%3E"
          )`,
          backgroundSize: "cover",
        }}
      >
        <div
          className={`flex justify-between items-center px-3 py-4 ${widthHeader}`}
        >
          <h1 className="lg:text-3xl md:text-xl text-lg font-bold uppercase">
            {route?.name}
          </h1>
          {isMobile ? null : (
            <div className="flex justify-center items-center">
              <Dropdown
                menu={{
                  items: userTab,
                }}
                trigger={["click"]}
              >
                <div
                  onClick={(e) => e.preventDefault()}
                  className="flex items-center w-full justify-center cursor-pointer space-x-2 z-20"
                >
                  <div className="flex justify-center items-center rounded-full bg-gray-300 w-8 h-8">
                    <Avatar src={userInfo?.avatar} />
                  </div>
                  <p className="text-lg font-medium capitalize">
                    {userInfo?.name}
                  </p>
                </div>
              </Dropdown>
            </div>
          )}
        </div>
        <div>{children}</div>
      </Layout>
    </Layout>
  );
}
