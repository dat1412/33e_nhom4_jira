import { Avatar, Dropdown } from "antd";
import React from "react";
import { useNavigate } from "react-router-dom";
import { userInfoLocal } from "../../services/local.service";
import { LogoutOutlined } from "@ant-design/icons";

export default function MobileMenu() {
  const profile = userInfoLocal.get();
  const navigate = useNavigate();
  const items = [
    {
      label: (
        <div className="flex items-center pb-2 border-b space-x-2 border-slate-600">
          <div className="flex justify-center items-center rounded-full bg-gray-300 w-8 h-8">
            <Avatar src={profile?.avatar} />
          </div>
          <p className="font-bold capitalize">{profile?.name}</p>
        </div>
      ),
      key: "0",
    },
    {
      label: (
        <span
          onClick={() => {
            navigate("/projects");
          }}
        >
          View all projects
        </span>
      ),
      key: "1",
    },
    {
      label: (
        <span
          onClick={() => {
            navigate("/project/new");
          }}
        >
          Create project
        </span>
      ),
      key: "2",
    },
    {
      label: (
        <span
          onClick={() => {
            navigate("/users");
          }}
        >
          View all users
        </span>
      ),
      key: "3",
    },
    {
      label: (
        <span
          onClick={() => {
            navigate("/task/new");
          }}
        >
          Create Task
        </span>
      ),
      key: "4",
    },
    {
      label: (
        <p
          className="flex items-center border-t border-slate-600 py-2 space-x-2 font-medium"
          onClick={() => {
            userInfoLocal.remove();
            navigate("/login");
          }}
        >
          <span className="inline-flex justify-center items-center text-xl text-red-500">
            <LogoutOutlined />
          </span>
          <span>Sign out</span>
        </p>
      ),
      key: "5",
    },
  ];

  return (
    <div
      style={{
        backgroundColor: "#001529",
      }}
      className="flex justify-between items-center w-full px-4 py-3 text-white z-20"
    >
      <div
        onClick={() => {
          navigate("/projects");
        }}
        className="w-8"
      >
        <img
          className="w-full"
          src="https://www.vectorlogo.zone/logos/atlassian_jira/atlassian_jira-icon.svg"
          alt="img-logo"
        />
      </div>
      <div className="text-white text-2xl">
        <Dropdown
          menu={{
            items,
          }}
          trigger={["click"]}
          placement="bottomLeft"
        >
          <div onClick={(e) => e.preventDefault()}>
            <i className="fa fa-bars"></i>
          </div>
        </Dropdown>
      </div>
    </div>
  );
}
