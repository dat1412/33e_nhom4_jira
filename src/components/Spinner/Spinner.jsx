import React from "react";
import { useSelector } from "react-redux";
import Lottie from "lottie-react";
import loadingAnimation from "../../assets/animate/loading-animation.json";

export default function Spinner() {
  let { isLoading } = useSelector((state) => {
    return state.spinnerSlice;
  });
  return isLoading ? (
    <div className="fixed top-0 right-0 w-screen h-screen z-50 bg-white">
      <div className="flex justify-center items-center w-full h-full">
        <Lottie
          className="w-1/5"
          animationData={loadingAnimation}
          loop={true}
        />
      </div>
    </div>
  ) : null;
}
