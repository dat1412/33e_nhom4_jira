import React, { useEffect } from "react";
import { LockOutlined, MailOutlined } from "@ant-design/icons";
import { Form, Input } from "antd";
import { NavLink, useNavigate } from "react-router-dom";
import Lottie from "lottie-react";
import loginAnimation from "../../assets/animate/login-animation.json";
import { userServices } from "../../services/user.service";
import { useDispatch } from "react-redux";
import { setUserInfo } from "../../redux/userSlice";
import { userInfoLocal } from "../../services/local.service";
import { checkLogin } from "../../utils/checkLogin/checkLogin.utils";
import { openNotification } from "../../utils/notification/notification";

export default function LoginPage() {
  let navigate = useNavigate();
  let dispatch = useDispatch();

  const onFinish = (values) => {
    // API
    userServices
      .postLogin(values)
      .then((res) => {
        // Set data user vao local
        userInfoLocal.set(res.data.content);

        // dispatch data user len redux
        dispatch(setUserInfo(res.data.content));
        openNotification(
          "success",
          "You have successfully logged in to your account!"
        );
        setTimeout(() => {
          navigate("/projects");
        }, 500);
      })
      .catch((err) => {
        openNotification("error", "An account failed to log on!");
      });
  };

  useEffect(() => {
    // Kiem tra dang nhap
    checkLogin(navigate, "projects");
  }, []);
  return (
    <div className="flex justify-center items-center h-screen w-screen bg-white-500">
      <div className="flex justify-center items-center text-center lg:w-1/2 lg:h-3/4 md:w-4/5 md:h-1/2 w-11/12 h-1/2 rounded-md shadow-2xl overflow-hidden bg-white">
        <div className="w-1/2 h-full flex justify-center items-center">
          <div className="w-3/4">
            <h1 className="lg:text-2xl md:text-2xl text-xl font-bold mb-8">
              Member Login
            </h1>
            <Form
              name="normal_login"
              className="login-form"
              onFinish={onFinish}
            >
              <Form.Item
                name="email"
                rules={[
                  {
                    type: "email",
                    message: "The input is not valid E-mail!",
                  },
                  {
                    required: true,
                    message: "Please input your E-mail!",
                  },
                ]}
              >
                <Input
                  prefix={<MailOutlined className="site-form-item-icon" />}
                  placeholder="Email"
                />
              </Form.Item>
              <Form.Item
                name="passWord"
                rules={[
                  {
                    required: true,
                    message: "Please input your Password!",
                  },
                ]}
              >
                <Input.Password
                  prefix={<LockOutlined className="site-form-item-icon" />}
                  type="password"
                  placeholder="Password"
                />
              </Form.Item>
              <Form.Item>
                <button className="login-form-button bg-emerald-600 text-white w-full p-2 rounded-md hover:bg-emerald-500">
                  LOG IN
                </button>
                <div className="mt-5">
                  <span>Not a member?</span>{" "}
                  <NavLink
                    className="text-emerald-600 hover:text-emerald-400"
                    to={"/register"}
                  >
                    Register now
                  </NavLink>
                </div>
              </Form.Item>
            </Form>
          </div>
        </div>
        <div className="w-1/2 h-full flex justify-center items-center bg-emerald-600">
          <Lottie animationData={loginAnimation} loop={true} />
        </div>
      </div>
    </div>
  );
}
