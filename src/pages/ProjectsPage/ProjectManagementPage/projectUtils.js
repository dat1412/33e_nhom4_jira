import { Avatar, Popconfirm, Tooltip } from "antd";
import { EditOutlined, DeleteOutlined } from "@ant-design/icons";
import { NavLink } from "react-router-dom";
import { openEditProject } from "../../../redux/editProjectSlice";

export const columnsProject = (
  dispatch,
  hanldeDeleteProject,
  filteredInfo,
  sortedInfo
) => {
  return [
    {
      title: "ID",
      dataIndex: "id",
      key: "id",
      sorter: (a, b) => a.id - b.id,
      sortOrder: sortedInfo.columnKey === "id" ? sortedInfo.order : null,
      ellipsis: true,
    },
    {
      title: "Project Name",
      dataIndex: "projectName",
      key: "projectName",
      sorter: (a, b) => a.projectName.length - b.projectName.length,
      sortOrder:
        sortedInfo.columnKey === "projectName" ? sortedInfo.order : null,
      ellipsis: true,
      render: (text, record, index) => {
        return (
          <NavLink
            className="text-blue-400 hover:text-blue-500"
            to={`/project/detail/${record.id}`}
          >
            {text}
          </NavLink>
        );
      },
    },
    {
      title: "Category Name",
      dataIndex: "categoryName",
      key: "categoryName",
      filters: [
        {
          text: "Web",
          value: "web",
        },
        {
          text: "Software",
          value: "phần mềm",
        },
        {
          text: "App",
          value: "di động",
        },
      ],
      filteredValue: filteredInfo.categoryName || null,
      onFilter: (value, record) => record.categoryName.includes(value),
      sorter: (a, b) => a.categoryName.length - b.categoryName.length,
      sortOrder:
        sortedInfo.columnKey === "categoryName" ? sortedInfo.order : null,
      ellipsis: true,
    },
    {
      title: "Creator",
      dataIndex: "creator",
      key: "creator",
      sorter: (a, b) => a.creator.name.length - b.creator.name.length,
      sortOrder: sortedInfo.columnKey === "creator" ? sortedInfo.order : null,
      ellipsis: true,
      render: (text, record, index) => {
        return <div>{text.name}</div>;
      },
    },
    {
      title: "Members",
      dataIndex: "members",
      key: "members",
      render: (text, record, index) => {
        return (
          <Avatar.Group
            maxCount={2}
            maxStyle={{
              color: "#f56a00",
              backgroundColor: "#fde3cf",
            }}
          >
            {text?.map((item) => {
              return (
                <Tooltip key={item.userId} title={item.name} placement="top">
                  <Avatar src={item.avatar} />
                </Tooltip>
              );
            })}
          </Avatar.Group>
        );
      },
    },
    {
      title: "Action",
      dataIndex: "action",
      key: "action",
      render: (text, record, index) => {
        return (
          <div key={index} className="flex justify-start items-center">
            <button
              onClick={() => {
                dispatch(openEditProject(record));
              }}
              className="inline-flex justify-center items-center text-lg text-yellow-400 hover:text-yellow-600 p-1"
            >
              <EditOutlined />
            </button>
            <button className="inline-flex justify-center items-center text-lg text-red-400 hover:text-red-600 p-1">
              <Popconfirm
                title="Are you sure to delete this Project?"
                placement="topRight"
                onConfirm={() => {
                  let params = {
                    projectId: record.id,
                  };
                  hanldeDeleteProject(params);
                }}
              >
                <DeleteOutlined />
              </Popconfirm>
            </button>
          </div>
        );
      },
    },
  ];
};
