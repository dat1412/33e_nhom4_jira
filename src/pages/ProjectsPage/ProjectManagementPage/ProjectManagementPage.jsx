import React, { useEffect, useState } from "react";
import {
  AutoComplete,
  Avatar,
  Button,
  Col,
  Input,
  message,
  Popconfirm,
  Row,
  Space,
  Table,
  Tooltip,
  Typography,
  List,
} from "antd";
import {
  SearchOutlined,
  EditOutlined,
  DeleteOutlined,
} from "@ant-design/icons";
import { useDispatch } from "react-redux";
import { NavLink, useNavigate } from "react-router-dom";
import { checkLogin } from "../../../utils/checkLogin/checkLogin.utils";
import { projectServices } from "../../../services/project.service";
import { openNotification } from "../../../utils/notification/notification";
import FormEditProject from "../../../components/Form/FormEditProject";
import { searchServices } from "../../../services/search.service";
import { columnsProject } from "./projectUtils";
import { openEditProject } from "../../../redux/editProjectSlice";

export default function ProjectManagementPage() {
  const [dataProject, setDataProject] = useState([]);
  let [keyword, setKeyword] = useState("");

  let navigate = useNavigate();
  let dispatch = useDispatch();

  const fetchDataProject = () => {
    projectServices
      .getAllProject()
      .then((res) => {
        setDataProject(res.data.content);
      })
      .catch((err) => {
        console.log("err: ", err);
      });
  };

  const hanldeSearchProject = () => {
    searchServices
      .getProject(keyword)
      .then((res) => {
        setDataProject(res.data.content);
      })
      .catch((err) => {
        console.log("err: ", err);
      });
  };

  const hanldeDeleteProject = (params) => {
    projectServices
      .deleteProject(params)
      .then((res) => {
        fetchDataProject();
        openNotification("success", "You have successfully deleted project!");
      })
      .catch((err) => {
        openNotification("error", "Did you fail at delete project!");
      });
  };

  useEffect(() => {
    // Kiem tra dang nhap
    checkLogin(navigate, "projects");

    // Gọi API
    fetchDataProject();
  }, []);

  useEffect(() => {
    hanldeSearchProject();
  }, [keyword]);

  // Start Table Antd
  const [filteredInfo, setFilteredInfo] = useState({});
  const [sortedInfo, setSortedInfo] = useState({});

  const handleChange = (pagination, filters, sorter) => {
    // console.log("Various parameters", pagination, filters, sorter);
    setFilteredInfo(filters);
    setSortedInfo(sorter);
  };

  const clearFilters = () => {
    setFilteredInfo({});
  };

  const clearAll = () => {
    setFilteredInfo({});
    setSortedInfo({});
  };

  const setIdSort = () => {
    setSortedInfo({
      order: "descend",
      columnKey: "id",
    });
  };
  // End Table Antd
  return (
    <div className="container mx-auto text-left">
      <Space
        style={{
          marginBottom: 16,
        }}
      >
        <AutoComplete
          onSearch={(value) => {
            setKeyword(value);
          }}
          className="w-40"
        >
          <Input prefix={<SearchOutlined />} placeholder="Search project" />
        </AutoComplete>

        <Button className="lg:block md:block hidden" onClick={setIdSort}>
          Sort ID
        </Button>
        <Button className="lg:block md:block hidden" onClick={clearFilters}>
          Clear filters
        </Button>
        <Button className="lg:block md:block hidden" onClick={clearAll}>
          Clear filters and sorters
        </Button>
      </Space>
      <Table
        className="lg:block md:block hidden"
        dataSource={dataProject}
        rowKey={"id"}
        onChange={handleChange}
        columns={columnsProject(
          dispatch,
          hanldeDeleteProject,
          filteredInfo,
          sortedInfo
        )}
      />
      <List
        className="lg:hidden md:hidden block"
        itemLayout="vertical"
        dataSource={dataProject}
        renderItem={(item) => (
          <List.Item>
            <Row className="mb-1" gutter={16}>
              <Col span={10}>
                <Typography.Text strong>Project name</Typography.Text>
              </Col>
              <Col span={14}>
                <NavLink
                  className="text-blue-400 hover:text-blue-500"
                  to={`/project/detail/${item.id}`}
                >
                  {item.projectName}
                </NavLink>
              </Col>
            </Row>

            <Row className="mb-1" gutter={16}>
              <Col span={10}>
                <Typography.Text strong>Category name</Typography.Text>
              </Col>
              <Col span={14}>{item.categoryName}</Col>
            </Row>

            <Row className="mb-1" gutter={16}>
              <Col span={10}>
                <Typography.Text strong>Creator</Typography.Text>
              </Col>
              <Col span={14}>{item.creator.name}</Col>
            </Row>

            <Row className="mb-1" gutter={16}>
              <Col span={10}>
                <Typography.Text strong>Members</Typography.Text>
              </Col>
              <Col span={14}>
                <Avatar.Group
                  maxCount={2}
                  maxStyle={{ color: "#f56a00", backgroundColor: "#fde3cf" }}
                >
                  {item.members.map((member) => (
                    <Tooltip title={member.name} key={member.userId}>
                      <Avatar src={member.avatar} />
                    </Tooltip>
                  ))}
                </Avatar.Group>
              </Col>
            </Row>
            <Row className="mb-1" gutter={16}>
              <Col span={10}>
                <Typography.Text strong>Actions</Typography.Text>
              </Col>
              <Col span={14}>
                <button
                  onClick={() => {
                    dispatch(openEditProject(item));
                  }}
                  className="inline-flex justify-center items-center text-lg text-yellow-400 hover:text-yellow-600 p-1"
                >
                  <EditOutlined />
                </button>
                <button className="inline-flex justify-center items-center text-lg text-red-400 hover:text-red-600 p-1">
                  <Popconfirm
                    title="Are you sure to delete this Project?"
                    placement="topRight"
                    onConfirm={() => {
                      let params = {
                        projectId: item.id,
                      };
                      hanldeDeleteProject(params);
                    }}
                  >
                    <DeleteOutlined />
                  </Popconfirm>
                </button>
              </Col>
            </Row>
          </List.Item>
        )}
      />
      <FormEditProject fetchDataProject={fetchDataProject} />
    </div>
  );
}
