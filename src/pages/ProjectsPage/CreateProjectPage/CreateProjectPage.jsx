import React, { useEffect } from "react";
import { Button, Form, Input, Select, Space } from "antd";
import { NavLink, useNavigate } from "react-router-dom";
import { projectServices } from "../../../services/project.service";
import { openNotification } from "../../../utils/notification/notification";
import { checkLogin } from "../../../utils/checkLogin/checkLogin.utils";

const { Option } = Select;

export default function CreateProjectPage() {
  let navigate = useNavigate();
  const [form] = Form.useForm();

  const onFinish = (values) => {
    // API
    projectServices
      .createProject(values)
      .then((res) => {
        openNotification(
          "success",
          "You have successfully create a new project!"
        );
      })
      .catch((err) => {
        openNotification("error", "Did you fail at create project!");
      });
  };

  const onReset = () => {
    form.resetFields();
  };

  useEffect(() => {
    checkLogin(navigate, "project/new");
  }, []);
  return (
    <div className="container mx-auto text-left w-3/4">
      <h3 className="text-2xl font-semibold mb-3">New Project</h3>
      <Form layout="vertical" form={form} onFinish={onFinish}>
        <Form.Item
          name="projectName"
          label="Project Name"
          rules={[
            {
              required: true,
              message: "Please input your project name!",
            },
          ]}
        >
          <Input placeholder="Please input your project name!" />
        </Form.Item>
        <Form.Item
          name="categoryId"
          label="Category Name"
          rules={[
            {
              required: true,
              message: "Please select your category name!",
            },
          ]}
        >
          <Select placeholder="Select a project category" allowClear>
            <Option value={1}>Web</Option>
            <Option value={2}>Software</Option>
            <Option value={3}>App</Option>
          </Select>
        </Form.Item>
        <Form.Item initialValue={""} name="description" label="Description">
          <Input.TextArea
            rows={7}
            placeholder="Please input your description!"
          />
        </Form.Item>
        <Form.Item>
          <Space>
            <Button type="primary" htmlType="submit">
              Create project
            </Button>
            <Button htmlType="button" onClick={onReset}>
              Reset form
            </Button>
          </Space>
        </Form.Item>
      </Form>
    </div>
  );
}
