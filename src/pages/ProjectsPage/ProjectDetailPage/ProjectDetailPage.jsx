import React, { useEffect, useState } from "react";
import {
  AutoComplete,
  Avatar,
  Input,
  Modal,
  Popconfirm,
  Popover,
  Tooltip,
} from "antd";
import { CloseOutlined, SearchOutlined, UserOutlined } from "@ant-design/icons";
import { NavLink, useNavigate, useParams } from "react-router-dom";
import { projectServices } from "../../../services/project.service";
import { checkLogin } from "../../../utils/checkLogin/checkLogin.utils";
import { openNotification } from "../../../utils/notification/notification";
import { searchServices } from "../../../services/search.service";
import { useDispatch } from "react-redux";
import TaskDetail from "./TaskDetail";
import {
  fetchPrioritesData,
  fetchProjectData,
  fetchStatusData,
  fetchTypesData,
} from "../../../middlewares/taskMiddleware";
import { Edit2 } from "iconsax-react";
import { DragDropContext, Draggable, Droppable } from "react-beautiful-dnd";
import { dragDropServices } from "../../../services/dragDrop.service";
import "../ProjectDetailPage/projectDetail.css";
import { taskService } from "../../../services/task.service";

export default function ProjectDetailPage() {
  let dispatch = useDispatch();

  // dispatch middlewares
  const dispatchMiddleWare = () => {
    dispatch(fetchProjectData());
    dispatch(fetchStatusData());
    dispatch(fetchPrioritesData());
    dispatch(fetchTypesData());
  };

  const [projectInfo, setProjectInfo] = useState({});
  const [listMembers, setListMembers] = useState([]);
  const [isOpenBacklog, setIsOpenBacklog] = useState(false);
  let [keyword, setKeyword] = useState("");
  let [taskeNameInput, setTaskNameInput] = useState("");
  let [typeInput, setTypeInput] = useState("01");

  let navigate = useNavigate();
  let { id } = useParams();
  const { lstTask, members } = projectInfo;

  // MODAL
  const [isModalOpen, setIsModalOpen] = useState(false);
  const [dataModal, setDataModal] = useState(null);
  const showModal = () => {
    setIsModalOpen(true);
  };

  const handleOk = () => {
    setIsModalOpen(false);
    setDataModal(null);
  };

  const handleCancel = () => {
    setIsModalOpen(false);
    setDataModal(null);
  };

  // API
  let fetchDetailProject = () => {
    let params = {
      id,
    };
    projectServices
      .getProjectDetail(params)
      .then((res) => {
        setProjectInfo(res.data.content);
      })
      .catch((err) => {
        console.log("err: ", err);
      });
  };

  let hanldeSearchMember = () => {
    searchServices
      .getUser(keyword)
      .then((res) => {
        setListMembers(res.data.content);
      })
      .catch((err) => {
        console.log("err: ", err);
      });
  };

  let handleRemoveUserFromProject = (data) => {
    projectServices
      .removeUserFromProject(data)
      .then((res) => {
        openNotification("success", "You have successfully deleted member!");
        fetchDetailProject();
      })
      .catch((err) => {
        openNotification("error", "Did you fail at delete member!");
      });
  };

  let handleAssignUserProject = (data) => {
    projectServices
      .assignUserProject(data)
      .then((res) => {
        openNotification("success", "You have successfully add member!");
        fetchDetailProject();
      })
      .catch((err) => {
        openNotification("error", "Did you fail at add member!");
      });
  };

  let renderMemberFormProject = () => {
    return members?.map((member) => {
      return (
        <div
          key={member.userId}
          className="flex items-center justify-between text-sm space-x-2 bg-gray-200 py-1 px-2 rounded z-20"
        >
          <Avatar size={28} src={member.avatar} />
          <span className="">{member.name}</span>
          <Popconfirm
            className="flex justify-center items-center text-red-400 cursor-pointer hover:text-red-600"
            title="Are you sure to delete this Member?"
            placement="topLeft"
            onConfirm={() => {
              let data = {
                projectId: id,
                userId: member.userId,
              };
              handleRemoveUserFromProject(data);
            }}
          >
            <CloseOutlined />
          </Popconfirm>
        </div>
      );
    });
  };

  let renderlistMembers = () => {
    return listMembers?.map((member) => {
      return (
        <div
          onClick={() => {
            let data = {
              projectId: id,
              userId: member.userId,
            };
            handleAssignUserProject(data);
          }}
          key={member.userId}
          className="flex items-center cursor-pointer text-sm space-x-2 bg-gray-200 py-1 px-2 rounded z-20"
        >
          <Avatar size={28} src={member.avatar} />
          <span className="">{member.name}</span>
        </div>
      );
    });
  };

  const onDragEnd = (result) => {
    const { destination, draggableId } = result;
    if (!destination) return;

    const data = {
      taskId: draggableId,
      statusId: destination.droppableId,
    };

    dragDropServices
      .updateStatus(data)
      .then((res) => {
        let params = {
          id,
        };
        dragDropServices
          .getProjectDetail(params)
          .then((res) => {
            setProjectInfo(res.data.content);
          })
          .catch((err) => {
            console.log("err: ", err);
          });
      })
      .catch((err) => {
        console.log("err: ", err);
      });
    return;
  };

  let renderListTask = () => {
    return lstTask?.map((task) => {
      return (
        <div key={task.statusId} className="card h-auto text-left">
          <div className="card-header">{task.statusName}</div>
          <Droppable droppableId={task.statusId}>
            {(provided) => (
              <ul
                {...provided.droppableProps}
                ref={provided.innerRef}
                className="list-group list-group-flush"
              >
                {task.lstTaskDeTail?.map((taskDeTail, index) => {
                  let classPriority = "1";

                  switch (taskDeTail.priorityTask.priority) {
                    case "Lowest":
                      classPriority = "priorityLowest";
                      break;
                    case "Low":
                      classPriority = "priorityLow";
                      break;
                    case "Medium":
                      classPriority = "priorityMedium";
                      break;
                    case "High":
                      classPriority = "priorityHigh";
                      break;

                    default:
                      break;
                  }

                  return (
                    <Draggable
                      key={index}
                      draggableId={taskDeTail.taskId.toString()}
                      index={index}
                    >
                      {(provided) => (
                        <li
                          ref={provided.innerRef}
                          {...provided.draggableProps}
                          {...provided.dragHandleProps}
                          onClick={() => {
                            showModal();
                            setDataModal(taskDeTail);
                          }}
                          className="list-group-item rounded shadow-sm transition-all cursor-pointer z-20"
                        >
                          <p className="text-sm">{taskDeTail.taskName}</p>
                          <div className="flex justify-between items-center">
                            <div className="block-left space-x-1 flex justify-center items-center">
                              <span className="text-lg">
                                {taskDeTail.taskTypeDetail.taskType ===
                                "bug" ? (
                                  <div className="text-red-500">
                                    <i className="fa fa-minus-square"></i>
                                  </div>
                                ) : (
                                  <div className="text-cyan-500">
                                    <i className="fa fa-check-square"></i>
                                  </div>
                                )}
                              </span>
                              <span
                                className={`text-xs font-medium rounded px-1 text-white ${classPriority}`}
                              >
                                {taskDeTail.priorityTask.priority}
                              </span>
                            </div>
                            <div className="flex justify-center items-center">
                              <Avatar.Group
                                className="flex justify-center items-center"
                                maxCount={2}
                                maxStyle={{
                                  color: "#f56a00",
                                  backgroundColor: "#fde3cf",
                                }}
                              >
                                {taskDeTail.assigness.length > 0 ? (
                                  taskDeTail.assigness.map((item) => {
                                    return (
                                      <Tooltip
                                        key={item.id}
                                        title={item.name}
                                        placement="top"
                                      >
                                        <Avatar src={item.avatar} />
                                      </Tooltip>
                                    );
                                  })
                                ) : (
                                  <Avatar
                                    className="flex justify-center items-center"
                                    icon={<UserOutlined />}
                                  />
                                )}
                              </Avatar.Group>
                            </div>
                          </div>
                        </li>
                      )}
                    </Draggable>
                  );
                })}
                {provided.placeholder}
              </ul>
            )}
          </Droppable>
          {task.statusId === "1" ? (
            !isOpenBacklog ? (
              <div
                onClick={() => {
                  setIsOpenBacklog(true);
                }}
                className="cursor-pointer hover:bg-slate-300 text-left text-base p-2 m-2 rounded space-x-3"
              >
                <span className="inline-flex justify-center items-center w-8 h-8 bg-slate-200 rounded-full">
                  <i className="fa fa-plus"></i>
                </span>
                <span>Create</span>
              </div>
            ) : (
              <div className="">
                <div
                  onClick={() => {
                    setIsOpenBacklog(false);
                  }}
                  className="fixed top-0 right-0 w-screen h-screen z-10"
                ></div>
                <div
                  className="m-2 rounded z-20 relative"
                  onKeyDown={(e) => {
                    const params = {
                      listUserAsign: [],
                      taskName: taskeNameInput,
                      description: "",
                      statusId: "1",
                      originalEstimate: 0,
                      timeTrackingSpent: 0,
                      timeTrackingRemaining: 0,
                      projectId: id,
                      typeId: typeInput,
                      priorityId: 2,
                    };
                    if (e.key === "Enter") {
                      taskService
                        .createTask(params)
                        .then((res) => {
                          openNotification(
                            "success",
                            "You have successfully created a task!"
                          );
                          setIsOpenBacklog(false);
                          fetchDetailProject();
                          setTaskNameInput("");
                          setTypeInput("01");
                        })
                        .catch((err) => {
                          openNotification(
                            "error",
                            "Did you fail at create a task!"
                          );
                          console.log("err: ", err);
                        });
                    }
                  }}
                >
                  <input
                    onChange={(e) => {
                      setTaskNameInput(e.target.value);
                    }}
                    className="block w-full h-12 p-2 z-20"
                    type="text"
                    placeholder="Please input your task name!"
                  />
                  <select
                    onChange={(e) => {
                      setTypeInput(e.target.value);
                    }}
                    defaultValue="01"
                    className="w-full p-2 z-20"
                  >
                    <option value="01">Bug</option>
                    <option value="02">Task</option>
                  </select>
                </div>
              </div>
            )
          ) : null}
          {dataModal ? (
            <Modal
              title={
                <div className="flex items-end mb-3">
                  <span className="text-xl font-bold mr-2">Edit task</span>
                  <Edit2 size="32" color="#FF7A03" variant="Bold" />
                </div>
              }
              open={isModalOpen}
              onOk={handleOk}
              onCancel={handleCancel}
              width={1000}
              footer={false}
            >
              <TaskDetail data={dataModal} func={{ handleOk, handleCancel }} />
            </Modal>
          ) : null}
        </div>
      );
    });
  };

  useEffect(() => {
    checkLogin(navigate, `project/detail/${id}`);
  }, []);

  useEffect(() => {
    fetchDetailProject();
    dispatchMiddleWare();
  }, [id]);

  useEffect(() => {
    hanldeSearchMember();
  }, [keyword]);
  return (
    <div className="container mx-auto text-left">
      <div className="main">
        <div className="z-20">
          <nav aria-label="breadcrumb">
            <ol className="breadcrumb">
              <li className="breadcrumb-item z-20">
                <NavLink to={"/projects"}>Project</NavLink>
              </li>
              <li className="breadcrumb-item active">
                {projectInfo.projectName}
              </li>
            </ol>
          </nav>
        </div>
        <div className="flex items-center justify-between">
          <div className="grid lg:grid-cols-5 md:grid-cols-2 gap-2">
            {renderMemberFormProject()}
          </div>
          <Popover
            className="text-center w-24 p-2 text-sm cursor-pointer text-blue-500 hover:text-blue-600 z-20"
            placement="bottom"
            title={"Add member"}
            trigger="click"
            content={() => {
              return (
                <div className="z-20">
                  <div className="px-1 mb-2">
                    <AutoComplete
                      onSearch={(value) => {
                        setKeyword(value);
                      }}
                      className="w-60"
                    >
                      <Input
                        prefix={<SearchOutlined />}
                        placeholder="Search member"
                      />
                    </AutoComplete>
                  </div>
                  <div className="h-40 space-y-2 overflow-auto px-1 scrollbar-thin scrollbar-thumb-rounded scrollbar-track-rounded scrollbar-thumb-gray-300 scrollbar-track-gray-100">
                    {renderlistMembers()}
                  </div>
                </div>
              );
            }}
          >
            + <span className="hover:underline z-20">Add more</span>
          </Popover>
        </div>
        <DragDropContext>
          <div className="content grid lg:grid-cols-4 gap-3 md:grid-cols-2 pb-4">
            <DragDropContext onDragEnd={onDragEnd}>
              {renderListTask()}
            </DragDropContext>
          </div>
        </DragDropContext>
      </div>
    </div>
  );
}
