import { message } from "antd";
import { Trash } from "iconsax-react";
import React from "react";
import { taskService } from "../../../../services/task.service";

export default function CommentDisplay({
  avatar,
  content,
  userName,
  userId,
  idComment,
  getAllComment,
  taskId,
}) {
  const deleteComment = async () => {
    try {
      await taskService.deleteComment({ idComment });
      message.success("Delete success");
      await getAllComment(taskId);
    } catch (error) {
      message.error(error.message);
    }
  };
  return (
    <div>
      <div className="w-full flex items-start my-3">
        <div className="w-10 h-10 mr-2 border-2 border-fuchsia-500 rounded-full overflow-hidden">
          <img
            className="w-full h-full object-cover object-center"
            src={
              avatar
                ? avatar
                : "https://ecdn.game4v.com/g4v-content/uploads/2019/08/12-5.jpg"
            }
            alt=""
          />
        </div>
        <div>
          <p className="text-lg font-medium">
            {userName ? userName : "userxxx"}
          </p>
          <p className="text-md font-thin italic text-slate-600">
            {content ? content : "hellohadsajhdsfhkjadfhk"}
          </p>
          <p>
            <span className=" cursor-pointer" onClick={deleteComment}>
              <Trash size="16" color="#f47373" />
            </span>
          </p>
        </div>
      </div>
    </div>
  );
}
