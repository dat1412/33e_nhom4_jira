import { Input, message } from "antd";
import { Send } from "iconsax-react";
import React, { useState } from "react";
import { userInfoLocal } from "../../../../services/local.service";
import { taskService } from "../../../../services/task.service";

export default function PostComment({ taskId, getAllComment }) {
  const userInfor = userInfoLocal.get();
  const [comment, setComment] = useState([]);
  const sendComment = async () => {
    try {
      let body = { taskId, contentComment: comment };
      await taskService.postComment(body);
      await getAllComment(taskId);
      message.success("Post thành công");
    } catch (error) {
      message.error(error.message);
    }
  };
  return (
    <div className="py-4">
      <div className="flex items-center">
        <di className="w-10 h-10 mr-2 rounded-full border-2 border-fuchsia-500 overflow-hidden">
          <img
            className="w-full h-full object-cover object-center"
            src={userInfor.avatar}
            alt=""
          />
        </di>
        <div>
          <p>{userInfor.name}</p>
          <div className="flex items-center">
            <div>
              <Input
                value={comment}
                onChange={(value) => {
                  setComment(value.target.value);
                }}
              />
            </div>
            <div className=" cursor-pointer" onClick={sendComment}>
              <Send size="32" color="#2ccce4" />
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
