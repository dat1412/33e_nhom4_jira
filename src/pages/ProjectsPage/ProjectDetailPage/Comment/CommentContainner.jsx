import React from "react";
import CommentDisplay from "./CommentDisplay";
import PostComment from "./PostComment";

export default function CommentContainner({
  commentList = [],
  getAllComment,
  taskId,
}) {
  return (
    <div className="w-full h-60 overflow-y-auto border border-slate-500 rounded-lg px-2 py-4">
      {commentList?.map((comment) => {
        const { userId, name, avatar } = comment.user;
        const { contentComment, id } = comment;
        return (
          <CommentDisplay
            taskId={taskId}
            userName={name}
            avatar={avatar}
            userId={userId}
            content={contentComment}
            idComment={id}
            getAllComment={getAllComment}
            key={id}
          />
        );
      })}
    </div>
  );
}
