import {
  Form,
  Button,
  Input,
  Select,
  InputNumber,
  Slider,
  Tag,
  message,
} from "antd";
import { Category, Profile2User, Status, Timer1 } from "iconsax-react";
import React, { useEffect, useState } from "react";
import { useSelector } from "react-redux";
import { taskService } from "../../../services/task.service";
import CommentContainner from "./Comment/CommentContainner";
import PostComment from "./Comment/PostComment";

export default function TaskDetail({ data, func }) {
  const [comment, setComment] = useState([]);
  const [trackingTime, setTrackingTime] = useState({
    timeTrackingRemaining: data.timeTrackingRemaining,
    timeTrackingSpent: data.timeTrackingSpent,
    originalEstimate: data.originalEstimate,
  });

  // Get data
  const getAllComment = async (taskId) => {
    try {
      const res = await taskService.getComment({ taskId });
      setComment(res.data.content);
    } catch (error) {
      message.error(error.message);
    }
  };

  // update task
  const updateTask = async (body) => {
    try {
      let res = await taskService.updateTask(body);
      func.handleOk();
      setTimeout(() => {
        message.success(res.data.message);
      }, 1000);
    } catch (error) {
      message.error(error.message);
    }
  };

  //   redux state
  const status = useSelector((state) => {
    return state.taskSlice.status;
  });
  const priorities = useSelector((state) => {
    return state.taskSlice.priorities;
  });

  const taskTypes = useSelector((state) => {
    return state.taskSlice.taskTypes;
  });

  const assignments = useSelector((state) => {
    return state.taskSlice.assignments;
  });

  const onFinish = (values) => {
    console.log("Success:", { ...values, projectId: data.projectId });
    let body = { ...values, projectId: data.projectId, taskId: data.taskId };
    updateTask(body);
  };
  const onFinishFailed = (errorInfo) => {
    console.log("Failed:", errorInfo);
  };

  useEffect(() => {
    getAllComment(data.taskId);
  }, []);

  return (
    <div>
      <Form
        name="basic"
        initialValues={{
          remember: true,
        }}
        onFinish={onFinish}
        onFinishFailed={onFinishFailed}
        autoComplete="off"
        layout="vertical"
      >
        <div className="flex">
          <div className="w-3/5 pr-2">
            <Form.Item
              name="typeId"
              label={
                <div className="flex items-center">
                  <Category color="#37d67a" variant="Bold" />
                  <span className="ml-2">Task type:</span>
                </div>
              }
              initialValue={data.taskTypeDetail.id}
            >
              <Select options={taskTypes} />
            </Form.Item>
            <Form.Item
              label="Task name:"
              name="taskName"
              initialValue={data.taskName}
              rules={[
                {
                  required: true,
                  message: "Please input task name!",
                },
              ]}
            >
              <Input />
            </Form.Item>
            <Form.Item
              label="Descriptions:"
              name="description"
              initialValue={data.description}
              rules={[
                {
                  required: true,
                  message: "Please input description!",
                },
              ]}
            >
              <Input.TextArea />
            </Form.Item>
            <CommentContainner
              taskId={data.taskId}
              commentList={comment}
              getAllComment={getAllComment}
            />
            <PostComment taskId={data.taskId} getAllComment={getAllComment} />
          </div>
          <div className="w-2/5 pl-2">
            <Form.Item
              name="statusId"
              label={
                <div className="flex items-center">
                  <Status color="#37d67a" variant="Bold" />
                  <span className="ml-2">Status:</span>
                </div>
              }
              initialValue={data.statusId}
            >
              <Select options={status} />
            </Form.Item>
            <Form.Item
              name="listUserAsign"
              label={
                <div className="flex items-center">
                  <Profile2User color="#E400FF" variant="Bold" />
                  <span className="ml-2">Assignees:</span>
                </div>
              }
              initialValue={data.assigness}
            >
              <Select options={assignments} />
            </Form.Item>
            <Form.Item
              name="priorityId"
              label="Priority:"
              initialValue={data.priorityTask.priorityId}
            >
              <Select options={priorities} />
            </Form.Item>

            <div className="grid grid-cols-2 gap-2">
              <Form.Item
                name="originalEstimate"
                label={
                  <div className="flex items-center">
                    <Timer1 color="#FA2E00" variant="Bold" />
                    <span className="ml-2">Total time Estimate:</span>
                  </div>
                }
                initialValue={data.originalEstimate}
              >
                <InputNumber
                  style={{ width: "100%" }}
                  type="number"
                  value={trackingTime.originalEstimate}
                  onChange={(value) => {
                    setTrackingTime({
                      ...trackingTime,
                      originalEstimate: value,
                      timeTrackingRemaining:
                        Number(value) - Number(trackingTime.timeTrackingSpent),
                    });
                  }}
                />
              </Form.Item>
              <Form.Item
                initialValue={trackingTime.timeTrackingSpent}
                name="timeTrackingSpent"
                label="Hours spent"
              >
                <InputNumber
                  type="number"
                  style={{ width: "100%" }}
                  value={trackingTime.timeTrackingSpent}
                  max={trackingTime.originalEstimate}
                  onChange={(value) => {
                    setTrackingTime({
                      ...trackingTime,
                      timeTrackingSpent: value,
                      timeTrackingRemaining:
                        Number(trackingTime.originalEstimate) - Number(value),
                    });
                  }}
                />
              </Form.Item>
            </div>

            <Slider
              defaultValue={trackingTime.timeTrackingSpent}
              value={trackingTime.timeTrackingSpent}
              max={trackingTime.originalEstimate}
              tooltip={{
                open: true,
              }}
            />
            <p className="text-right font-medium text-lg">
              Time remaining {trackingTime.timeTrackingRemaining} hours
            </p>
          </div>
        </div>
        <Form.Item>
          <Button type="primary" htmlType="submit">
            Submit
          </Button>
        </Form.Item>
      </Form>
    </div>
  );
}
