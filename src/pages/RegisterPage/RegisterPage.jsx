import React from "react";
import {
  LockOutlined,
  MailOutlined,
  UserOutlined,
  PhoneOutlined,
} from "@ant-design/icons";
import { Form, Input } from "antd";
import { NavLink, useNavigate } from "react-router-dom";
import Lottie from "lottie-react";
import registerAnimation from "../../assets/animate/register-animation.json";
import { userServices } from "../../services/user.service";
import { openNotification } from "../../utils/notification/notification";

export default function RegisterPage() {
  let navigate = useNavigate();
  const onFinish = (values) => {
    // API
    userServices
      .postRegister(values)
      .then((res) => {
        openNotification(
          "success",
          "You have successfully registered an account!"
        );
        setTimeout(() => {
          navigate("/login");
        }, 500);
      })
      .catch((err) => {
        openNotification("error", "Did you fail at account registration!");
      });
  };

  return (
    <div className="flex justify-center items-center h-screen w-screen bg-white-500">
      <div className="flex justify-center items-center text-center lg:w-1/2 lg:h-3/4 md:w-4/5 md:h-3/5 w-11/12 h-4/5 rounded-md shadow-2xl overflow-hidden bg-white">
        <div className="w-1/2 h-full flex justify-center items-center bg-emerald-600">
          <Lottie animationData={registerAnimation} loop={true} />
        </div>
        <div className="w-1/2 h-full flex justify-center items-center">
          <div className="lg:w-3/4 md:w-3/4 w-11/12">
            <h1 className="lg:text-2xl md:text-2xl text-xl font-bold mb-8">
              Member Registration
            </h1>
            <Form
              name="normal_register"
              className="register-form"
              onFinish={onFinish}
            >
              <Form.Item
                name="name"
                rules={[
                  {
                    required: true,
                    message: "Please input your name!",
                    whitespace: true,
                  },
                ]}
              >
                <Input
                  prefix={<UserOutlined className="site-form-item-icon" />}
                  placeholder="Name"
                />
              </Form.Item>
              <Form.Item
                name="email"
                rules={[
                  {
                    type: "email",
                    message: "The input is not valid E-mail!",
                  },
                  {
                    required: true,
                    message: "Please input your E-mail!",
                  },
                ]}
              >
                <Input
                  prefix={<MailOutlined className="site-form-item-icon" />}
                  placeholder="Email"
                />
              </Form.Item>
              <Form.Item
                name="passWord"
                rules={[
                  {
                    required: true,
                    message: "Please input your password!",
                  },
                ]}
              >
                <Input.Password
                  prefix={<LockOutlined className="site-form-item-icon" />}
                  type="password"
                  placeholder="Password"
                />
              </Form.Item>

              <Form.Item
                name="phoneNumber"
                rules={[
                  {
                    required: true,
                    message: "Please input your phone number!",
                  },
                ]}
              >
                <Input
                  prefix={<PhoneOutlined className="site-form-item-icon" />}
                  placeholder="Phone Number"
                />
              </Form.Item>
              <Form.Item>
                <button className="bg-emerald-600 text-white w-full p-2 rounded-md hover:bg-emerald-500">
                  REGISTER
                </button>
                <div className="mt-5">
                  <span>Have already an account?</span>{" "}
                  <NavLink
                    className="text-emerald-600 hover:text-emerald-400"
                    to={"/login"}
                  >
                    Login here
                  </NavLink>
                </div>
              </Form.Item>
            </Form>
          </div>
        </div>
      </div>
    </div>
  );
}
