import React, { useEffect } from "react";
import { useNavigate } from "react-router-dom";
import Lottie from "lottie-react";
import loadingAnimation from "../../assets/animate/loading-animation.json";
import { checkLogin } from "../../utils/checkLogin/checkLogin.utils";

export default function HomePage() {
  let navigate = useNavigate();

  useEffect(() => {
    checkLogin(navigate, "projects");
  }, []);

  return (
    <div className="fixed top-0 right-0 w-screen h-screen z-50 bg-white">
      <div className="flex justify-center items-center w-full h-full">
        <Lottie
          className="w-1/5"
          animationData={loadingAnimation}
          loop={true}
        />
      </div>
    </div>
  );
}
