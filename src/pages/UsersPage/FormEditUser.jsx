import React from "react";
import { Button, Checkbox, Form, Input } from "antd";
import { data } from "autoprefixer";
import { userController } from "../../controller/user.controller";

export default function FormEditUser({ data, handleSubmit }) {
  const onFinish = async (values) => {
    let dataUser = { ...values, id: data.userId };
    if (!data.passWord) {
      delete data.passWord;
    }
    await userController.editUser(dataUser);
    handleSubmit();
  };
  const onFinishFailed = (errorInfo) => {
    console.log("Failed:", errorInfo);
  };
  return (
    <Form
      name="basic"
      layout="vertical"
      onFinish={onFinish}
      onFinishFailed={onFinishFailed}
      autoComplete="off"
    >
      <Form.Item initialValue={data.name} label="Name" name="name">
        <Input />
      </Form.Item>

      <Form.Item initialValue={data.email} label="Email" name="email">
        <Input />
      </Form.Item>

      <Form.Item
        initialValue={data.phoneNumber}
        label="Phone number"
        name="phoneNumber"
      >
        <Input />
      </Form.Item>

      <Form.Item initialValue={data.passWord} label="Password" name="passWord">
        <Input.Password />
      </Form.Item>

      <Form.Item>
        <Button
          className="bg-sky-800 text-white float-right"
          type="primary"
          htmlType="submit"
        >
          Submit
        </Button>
      </Form.Item>
    </Form>
  );
}
