import React, { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import { checkLogin } from "../../utils/checkLogin/checkLogin.utils";
import { userController } from "../../controller/user.controller";
import TableUser from "./TableUser";

export default function UsersPage() {
  let navigate = useNavigate();
  let [userData, setUserData] = useState([]);

  const fetchData = async () => {
    // Kiem tra dang nhap
    checkLogin(navigate, "users");
    const listUser = await userController.getAllUsers();
    setUserData(listUser);
  };

  useEffect(() => {
    fetchData();
  }, []);
  return (
    <div className=" px-3">
      <TableUser
        data={userData}
        userFunc={userController}
        fetchData={fetchData}
      />
    </div>
  );
}
