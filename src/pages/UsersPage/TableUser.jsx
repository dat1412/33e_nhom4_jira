import React, { useState } from "react";
import { Input, message, Modal, Table } from "antd";
import FormEditUser from "./FormEditUser";

export default function TableUser({ data = [], userFunc, fetchData }) {
  const [dataUserEditProp, setDataUserEditProp] = useState({
    name: "Khang",
    email: "",
    phoneNumber: "",
    passWord: "",
  });
  const [sortedInfo, setSortedInfo] = useState({});
  const [isModalOpen, setIsModalOpen] = useState(false);
  const [isApear, setIsApear] = useState(false);
  const [dataState, setDataState] = useState(null);
  const [searchValue, setSearchValue] = useState("");
  const handleChange = (pagination, filters, sorter) => {
    setSortedInfo(sorter);
  };

  const clearSort = () => {
    setSortedInfo({});
  };
  const clearAll = () => {
    setSortedInfo({});
    setSearchValue("");
    setDataState(null);
  };
  const showModal = () => {
    setIsModalOpen(true);
    setIsApear(true);
  };
  const closeModel = () => {
    setIsModalOpen(false);
  };
  const handleSubmit = async () => {
    await fetchData();
    message.success("Chỉnh sửa thành công");
    closeModel();
  };
  const onChangeSearch = (e) => {
    let { value } = e.target;
    setSearchValue(value);
    let cloneData = [...data];
    if (value.split("") !== []) {
      value.split("").forEach((char, index) => {
        cloneData = cloneData.filter((item) => item.name[index] === char);
      });
    } else {
      cloneData = null;
    }
    setDataState(cloneData);
  };

  const columns = [
    {
      title: "ID",
      dataIndex: "userId",
      key: "userId",
      sorter: (a, b) => a.userId - b.userId,
      sortOrder: sortedInfo.columnKey === "userId" ? sortedInfo.order : null,
      ellipsis: true,
    },
    {
      title: "Avatar",
      dataIndex: "avatar",
      key: "avatar",
      render: (url) => {
        return (
          <div>
            <img style={{ width: 40, height: 40 }} src={url} alt="" />
          </div>
        );
      },
    },
    {
      title: "Name",
      dataIndex: "name",
      key: "name",
      sorter: (a, b) => a.name.length - b.name.length,
      sortOrder: sortedInfo.columnKey === "name" ? sortedInfo.order : null,
      ellipsis: true,
    },
    {
      title: "Email",
      dataIndex: "email",
      key: "email",
    },
    {
      title: "Phone number",
      dataIndex: "phoneNumber",
      key: "phoneNumber",
    },
    {
      title: "Action",
      render: (currentUser) => {
        return (
          <div>
            <button
              onClick={() => {
                setDataUserEditProp(currentUser);
                showModal();
              }}
              className="text-xl text-lime-500 mr-6 hover:text-lime-700 hover:scale-125 transition duration-300"
            >
              <i className="fa fa-pencil-ruler"></i>
            </button>
            <button
              onClick={async () => {
                await userFunc.deleteUser(currentUser.userId);
                await fetchData();
              }}
              className="text-xl text-red-500 hover:text-red-700 hover:scale-125 transition duration-300"
            >
              <i className="fa fa-trash-alt"></i>
            </button>
          </div>
        );
      },
    },
  ];
  return (
    <>
      <div>
        <div className="flex items-center mb-6">
          <div>
            <Input
              placeholder="input search text"
              allowClear
              value={searchValue}
              onChange={onChangeSearch}
              size="large"
              style={{
                width: "100%",
              }}
            />
          </div>
          <div>
            <button
              className="ml-2 border px-4 py-2 rounded-full bg-white text-slate-400 hover:text-sky-700"
              onClick={clearSort}
            >
              Clear sorters
            </button>
          </div>
          <div>
            <button
              className="ml-2 border px-4 py-2 rounded-full bg-white text-slate-400 hover:text-sky-700"
              onClick={clearAll}
            >
              Clear All
            </button>
          </div>
        </div>
      </div>
      <div>
        {isApear ? (
          <Modal
            title="Edit user information"
            open={isModalOpen}
            footer={false}
            onCancel={closeModel}
            afterClose={() => {
              setIsApear(false);
            }}
          >
            <FormEditUser data={dataUserEditProp} handleSubmit={handleSubmit} />
          </Modal>
        ) : null}
      </div>

      <Table
        size="large"
        scroll={{ x: 768 }}
        columns={columns}
        dataSource={dataState ? dataState : data}
        onChange={handleChange}
      />
    </>
  );
}
