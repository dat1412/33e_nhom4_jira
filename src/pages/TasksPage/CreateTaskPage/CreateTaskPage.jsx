import React, { useEffect, useState } from "react";
import {
  Button,
  Form,
  Input,
  InputNumber,
  message,
  Select,
  Slider,
} from "antd";
import { useNavigate } from "react-router-dom";
import { checkLogin } from "../../../utils/checkLogin/checkLogin.utils";
import { taskService } from "../../../services/task.service";
import { useDispatch, useSelector } from "react-redux";
import { getAssignment } from "../../../redux/taskSlice";
import {
  fetchPrioritesData,
  fetchProjectData,
  fetchStatusData,
  fetchTypesData,
} from "../../../middlewares/taskMiddleware";

import {
  Book1,
  Menu,
  Star,
  Status,
  Timer,
  TimerStart,
  User,
} from "iconsax-react";

export default function CreateTaskPage() {
  let navigate = useNavigate();
  let [trackingTime, setTrackingTime] = useState({
    timeTrackingRemaining: 0,
    timeTrackingSpent: 0,

    originalEstimate: 0,
  });

  let projects = useSelector((state) => {
    return state.taskSlice.projects;
  });

  let projectData = useSelector((state) => {
    return state.taskSlice.projectData;
  });

  let status = useSelector((state) => {
    return state.taskSlice.status;
  });

  let priorities = useSelector((state) => {
    return state.taskSlice.priorities;
  });

  let taskTypes = useSelector((state) => {
    return state.taskSlice.taskTypes;
  });

  let assignments = useSelector((state) => {
    return state.taskSlice.assignments;
  });

  const dispatch = useDispatch();

  const onFinish = (data) => {
    const createTask = async (body) => {
      try {
        body = {
          ...body,
          timeTrackingRemaining: trackingTime.timeTrackingRemaining,
        };

        let res = await taskService.createTask(body);
        message.success(res.data.message);
        setTimeout(() => {
          window.location.reload(false);
        }, 1000);
      } catch (error) {
        message.error(error.message);
      }
    };
    createTask(data);
  };
  const onFinishFailed = (errorInfo) => {
    // console.log("Failed:", errorInfo);
  };

  const fetchDataApi = () => {
    dispatch(fetchProjectData());
    dispatch(fetchStatusData());
    dispatch(fetchPrioritesData());
    dispatch(fetchTypesData());
  };

  const handleChangeProject = (value) => {
    dispatch(getAssignment({ projectId: value, projectData }));
  };

  useEffect(() => {
    fetchDataApi();
    checkLogin(navigate, "task/new");
  }, []);
  return (
    <div className="lg:py-8 md:py-6 py-4">
      <div
        className="w-3/4 mx-auto px-4 py-2 shadow-lg"
        style={{
          background: "rgba( 255, 255, 255, 0.25 )",
          boxShadow: "0 8px 32px 0 rgba( 31, 38, 135, 0.37 )",
          backdropFilter: "blur( 13px )",
          WebkitBackdropFilter: " blur( 13px )",
          borderRadius: " 10px",
          border: "1px solid rgba( 255, 255, 255, 0.18 )",
        }}
      >
        <Form
          name="basic"
          initialValues={{
            remember: true,
          }}
          layout="vertical"
          onFinish={onFinish}
          onFinishFailed={onFinishFailed}
          autoComplete="off"
        >
          <Form.Item
            initialValue={projects[0].value}
            name="projectId"
            label={
              <p className="flex items-center">
                <Book1 color="#FF5733" variant="Bold" />{" "}
                <span className="ml-2">Project :</span>
              </p>
            }
          >
            <Select onChange={handleChangeProject} options={projects} />
          </Form.Item>

          <Form.Item
            initialValue=""
            name="taskName"
            label="Task name"
            rules={[
              {
                required: true,
                message: "Please input task name!",
              },
            ]}
          >
            <Input />
          </Form.Item>

          <Form.Item
            initialValue={"1"}
            name="statusId"
            label={
              <p className="flex items-center">
                <Status color="#37d67a" variant="Bold" />{" "}
                <span className="ml-2">Status :</span>
              </p>
            }
          >
            <Select options={status} />
          </Form.Item>

          <div className="grid grid-cols-2 gap-2">
            <Form.Item
              initialValue={1}
              name="priorityId"
              label={
                <p className="flex items-center">
                  <Star color="#FFBF00 " variant="Bold" />{" "}
                  <span className="ml-2">Project :</span>
                </p>
              }
            >
              <Select options={priorities} />
            </Form.Item>
            <Form.Item
              initialValue={1}
              name="typeId"
              label={
                <p className="flex items-center">
                  <Menu color="#FF00FF " variant="Bold" />{" "}
                  <span className="ml-2">Task type :</span>
                </p>
              }
            >
              <Select options={taskTypes} />
            </Form.Item>
          </div>

          <Form.Item
            initialValue={[]}
            name="listUserAsign"
            label={
              <p className="flex items-center">
                <User color="#6495ED" variant="Bold" />{" "}
                <span className="ml-2">Assignees :</span>
              </p>
            }
          >
            <Select mode="multiple" options={assignments} />
          </Form.Item>

          <div className="grid grid-cols-2 gap-2">
            <Form.Item
              initialValue={0}
              name="originalEstimate"
              label={
                <p className="flex items-center">
                  <TimerStart color="#FF7F50" variant="Bold" />{" "}
                  <span className="ml-2">Total Estimated Hours :</span>
                </p>
              }
            >
              <InputNumber
                type="number"
                min={0}
                style={{ width: "100%" }}
                value={trackingTime.originalEstimate}
                onChange={(value) => {
                  setTrackingTime({
                    ...trackingTime,
                    originalEstimate: value,
                    timeTrackingRemaining:
                      Number(value) - Number(trackingTime.timeTrackingSpent),
                  });
                }}
              />
            </Form.Item>
            <Form.Item
              initialValue={0}
              name="timeTrackingSpent"
              label={
                <p className="flex items-center">
                  <Timer color="#40E0D0" variant="Bold" />{" "}
                  <span className="ml-2">Hours spent :</span>
                </p>
              }
            >
              <InputNumber
                type="number"
                min={0}
                style={{ width: "100%" }}
                value={trackingTime.timeTrackingSpent}
                max={trackingTime.originalEstimate}
                onChange={(value) => {
                  setTrackingTime({
                    ...trackingTime,
                    timeTrackingSpent: value,
                    timeTrackingRemaining:
                      Number(trackingTime.originalEstimate) - Number(value),
                  });
                }}
              />
            </Form.Item>
          </div>

          <Slider
            defaultValue={trackingTime.timeTrackingSpent}
            value={trackingTime.timeTrackingSpent}
            max={trackingTime.originalEstimate}
            tooltip={{
              open: true,
            }}
          />

          <p className="text-sm font-bold text-right">
            Time remaining: {trackingTime.timeTrackingRemaining} (hour)
          </p>

          <Form.Item
            initialValue={""}
            name="description"
            label="Description"
            rules={[
              {
                required: true,
                message: "Please input description!",
              },
            ]}
          >
            <Input.TextArea />
          </Form.Item>

          <Form.Item>
            <Button type="primary" htmlType="submit">
              Submit
            </Button>
          </Form.Item>
        </Form>
      </div>
    </div>
  );
}
