import React, { useEffect, useState } from "react";
import JiraTag from "../../../components/JiraTag";
import { projectServices } from "../../../services/project.service";

export default function TaskDetailPage() {
  let [taskDetail, setTaskDetail] = useState([]);
  const getTaskDetail = async () => {
    let res = await projectServices.getProjectDetail({ id: 10295 });
    console.log("res: ", res.data.content);
    setTaskDetail(res.data.content);
  };
  useEffect(() => {
    getTaskDetail();
  }, []);
  return (
    <div className="px-3 py-4">
      <JiraTag
        title={
          <div>
            <p className="text-xl">{taskDetail?.projectName}</p>
            <p className="text-sm">Creator: {taskDetail?.creator?.name}</p>
          </div>
        }
        content={<p>{taskDetail?.description}</p>}
      />
    </div>
  );
}
