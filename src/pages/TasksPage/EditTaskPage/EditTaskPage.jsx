import React from "react";
import { useNavigate } from "react-router-dom";
import { checkLogin } from "../../../utils/checkLogin/checkLogin.utils";

export default function EditTaskPage() {
  let navigate = useNavigate();
  useEffect(() => {
    checkLogin(navigate, "task/new");
  }, []);
  return <div></div>;
}
