import { createSlice } from "@reduxjs/toolkit";

let initialState = {
  isOpen: false,
  detailBacklog: null,
};

const addDetailBacklogSlice = createSlice({
  name: "addDetailBacklogSlice",
  initialState,
  reducers: {
    openAddDetailBacklog: (state, aciton) => {
      return { ...state, isOpen: true, detailBacklog: aciton.payload };
    },
    closeAddDetailBacklog: (state) => {
      return { ...state, isOpen: false };
    },
  },
});

export const { openAddDetailBacklog, closeAddDetailBacklog } =
  addDetailBacklogSlice.actions;
export default addDetailBacklogSlice.reducer;
