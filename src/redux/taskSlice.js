import { createSlice } from "@reduxjs/toolkit";
import {
  fetchPrioritesData,
  fetchProjectData,
  fetchStatusData,
  fetchTypesData,
} from "../middlewares/taskMiddleware";

const initialState = {
  projects: [{ value: 0, label: "...choose your project" }],
  projectData: [],
  status: [],
  priorities: [],
  taskTypes: [],
  assignments: [],
};

export const taskSlice = createSlice({
  name: "projects",
  initialState,
  reducers: {
    getAssignment: (state, action) => {
      let { projectId, projectData } = action.payload;
      let index = projectData.findIndex((project) => project.id === projectId);
      let project = index !== -1 ? projectData[index] : null;
      const setAssignments = (project) => {
        return project.members.map((member) => {
          return { value: member.userId, label: member.name };
        });
      };
      state.assignments = project ? setAssignments(project) : [];
    },
  },
  extraReducers: (builder) => {
    builder
      .addCase(fetchProjectData.fulfilled, (state, action) => {
        let projectList = action.payload.map((project) => {
          return { value: project.id, label: project.projectName };
        });
        state.projectData = action.payload;
        state.projects = projectList;
      })
      .addCase(fetchStatusData.fulfilled, (state, action) => {
        let statusList = action.payload.map((status) => {
          return { value: status.statusId, label: status.statusName };
        });
        state.status = statusList;
      })
      .addCase(fetchPrioritesData.fulfilled, (state, action) => {
        let prioritiesList = action.payload.map((priority) => {
          return { value: priority.priorityId, label: priority.priority };
        });
        state.priorities = prioritiesList;
      })
      .addCase(fetchTypesData.fulfilled, (state, action) => {
        let taskTypesList = action.payload.map((type) => {
          return { value: type.id, label: type.taskType };
        });
        state.taskTypes = taskTypesList;
      });
  },
});

export const { getAssignment } = taskSlice.actions;

export default taskSlice.reducer;
