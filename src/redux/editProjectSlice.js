import { createSlice } from "@reduxjs/toolkit";

let initialState = {
  isOpen: false,
  projectInfo: null,
};

const editProjectSlice = createSlice({
  name: "editProjectSlice",
  initialState,
  reducers: {
    openEditProject: (state, aciton) => {
      return { ...state, isOpen: true, projectInfo: aciton.payload };
    },
    closeEditProject: (state) => {
      return { ...state, isOpen: false };
    },
  },
});

export const { openEditProject, closeEditProject } = editProjectSlice.actions;
export default editProjectSlice.reducer;
